//
//  VideoCell.h
//  Tutorials
//
//  Created by p.mitev on 20.02.17.
//  Copyright © 2017 Petar Mitev. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Video;


@interface VideoCell : UITableViewCell
-(void)updateUI:(nonnull Video*)video;
@end
