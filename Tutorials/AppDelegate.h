//
//  AppDelegate.h
//  Tutorials
//
//  Created by p.mitev on 16.02.17.
//  Copyright © 2017 Petar Mitev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

