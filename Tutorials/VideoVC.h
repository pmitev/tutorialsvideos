//
//  VideoVC.h
//  Tutorials
//
//  Created by p.mitev on 24.02.17.
//  Copyright © 2017 Petar Mitev. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Video;

@interface VideoVC : UIViewController<UIWebViewDelegate>
@property (nonatomic, strong) Video *video;

@end
