//
//  HTTP-Service.h
//  Tutorials
//
//  Created by p.mitev on 16.02.17.
//  Copyright © 2017 Petar Mitev. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^onComplete)(NSArray *  _Nullable dataArray,NSString * _Nullable errMessage );
@interface HTTP_Service : NSObject
+ (id) instance;
- (void) getTutorials:(nullable onComplete)completionHandler;
@end
