//
//  HTTP-Service.m
//  Tutorials
//
//  Created by p.mitev on 16.02.17.
//  Copyright © 2017 Petar Mitev. All rights reserved.
//

#import "HTTP-Service.h"
#define URL_BASE "http://localhost:6069"
#define URL_TUTORIALS "/tutorials"

@implementation HTTP_Service

+ (id) instance {

    static HTTP_Service *sharedInstance = nil;
    
    @synchronized (self) {
        if (sharedInstance == nil)
            sharedInstance = [[self alloc]init];
    }
 
    return sharedInstance;
}

- (void) getTutorials:(nullable onComplete)completionHandler {
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%s%s", URL_BASE , URL_TUTORIALS]];
    NSURLSession *session = [NSURLSession sharedSession];
    
    [[session dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (data != nil){
            NSError *err;
            NSArray *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&err];
        
            if (err == nil){
            
                completionHandler(json,nil);
                
            }else{
            
                completionHandler(nil, @"There is no data!");
            
            }
            
        
        } else {
            
           NSLog(@"Error: %@", error.debugDescription);
            completionHandler(nil,@"Problem connecting to server!");
        
            
    }
    
    }] resume ];
}

@end
